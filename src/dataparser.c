#include <dataparser.h>

void linesToNums(int numLines, char **lines, short sizeNum, unsigned long* nums)
{
	int i;
	for (i = 0; i < numLines; i++)
	{
		nums[i] = (int) strtol(lines[i], '\0', 2);
	}
}

void bucketing(int numLines, int sizeNum, unsigned long* nums, int* buckets)
{
	int i, j;
	
	for (j = 0; j < sizeNum; j++)
	{
		buckets[j] = 0;
	}
	for (i = 0; i < numLines; i++)
	{
		for (j = 0; j < sizeNum; j++)
		{
			if (nums[i] & (1 << j))
			{
				buckets[j]++;
			}
		}
	}
}

void findCandidateBits(int numLines, short sizeNum, int* buckets, int* candidateBits)
{
	int i;
	int j = 0;
	int cur, swap;
	double expected = (double)numLines / 2.0;
	
	//I know...this is insertion sort...and it's O(n^2)....but n is the bit length, so it's not too bad.
	//If it's a problem we can fix it later.
	for (i = 0; i < sizeNum; i++)
	{
		candidateBits[i] = -1;
	}
	for (i = 0; i < sizeNum; i++)
	{
		cur = i;
		j = 0;
		while (candidateBits[j] >= 0)
		{
			if (fabs((double)buckets[cur] - expected) < fabs((double)buckets[candidateBits[j]] - expected))
			{
				swap = candidateBits[j];
				candidateBits[j] = cur;
				cur = swap;
			}
			j++;
		}
		candidateBits[j] = cur;
	}
}

void buildCandidateNums(int numLines, int tolerance, unsigned long* nums, int* candidateBits, unsigned long* candidateNums)
{
	int i, j;
	int bit;
	for (i = 0; i < numLines; i++)
	{
		for (j = 0; j < tolerance; j++)
		{
			bit = (nums[i] & (1 << candidateBits[j])) > 0;
			candidateNums[i] += (bit << j);
		}
	}
	
}











