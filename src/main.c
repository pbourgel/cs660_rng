/*
 * main.c
 *
 *  Created on: Oct 12, 2012
 *      Author: peterbourgelais
 */
#include <stdio.h>
#include <stdlib.h>
#include <filehandler.h>
#include <dataparser.h>
#include <chisquare.h>

int main(int argc, char** argv)
{
	if (argc < 5)
	{
		printf("./run inFile numLines sizeNum tolerance\n");
		exit(1);
	}
	
	/*Argument Variables*/
	char* inFileName = argv[1];
	int numLines = atoi(argv[2]);
	int sizeNum = atoi(argv[3]);
	int tolerance = atoi(argv[4]);
	
	if (tolerance > sizeNum)
	{
		printf("ERROR: Tolerance (%d) cannot be larger than sizeNum (%d)\n", tolerance, sizeNum);
		exit(1);
	}
	
	/*Arrays needing malloc*/
	char** lines = (char**) malloc(sizeof(char) * (sizeNum + 1) * numLines * 10);
	unsigned long* nums = (unsigned long*) malloc(sizeof(unsigned long) * numLines * 10000);
	int* buckets = (int*) malloc(sizeof(int) * sizeNum);
	int* candidateBits = (int*) malloc(sizeof(int) * sizeNum);
	unsigned long* candidateNums = (unsigned long*) malloc(sizeof(unsigned long) * numLines * 10000);
	
	/*Utility*/
	int i;
	
	/*To-Be-Defined variables*/
	double chiSquare;
	int chiSquareRandom;
	unsigned long int maxNum;
	double bound;
	
	FILE *in = fopen(inFileName, "r");
	readFile(in, numLines, lines);
	fclose(in);
	
	linesToNums(numLines, lines, sizeNum, nums);
	bucketing(numLines, sizeNum, nums, buckets);
	
	findCandidateBits(numLines, sizeNum, buckets, candidateBits);
	buildCandidateNums(numLines, tolerance, nums, candidateBits, candidateNums);
	
	chiSquare = chiSquare_calc(numLines, tolerance, candidateNums);
	chiSquareRandom = chiSquare_isRandom(chiSquare, tolerance);
	
	
	maxNum = (unsigned long) (pow(2, tolerance) - 1);
	bound = 2*sqrt(pow(2, tolerance) - 1);
	printf("Using best %d bits:\n", tolerance);
	printf("\tBits: ");
	for (i = 0; i < tolerance; i++)
	{
		printf("%d ", candidateBits[i]);
	}
	printf("\n");
	printf("\tChiSquare: %f\n", chiSquare );
	printf("\tIdeal: %lu\n", maxNum);
	printf("\tDifference: %.02f%%\n", 100 * fabs(maxNum - chiSquare) / maxNum);
	printf("\tRandom Range: %f to %f \n", maxNum - bound, maxNum + bound );
	printf("\tIsRandom: %d\n", chiSquareRandom);
	printf("------\n");
	
	chiSquare = chiSquare_calc(numLines, sizeNum, nums);
	chiSquareRandom = chiSquare_isRandom(chiSquare, sizeNum);
	
	maxNum = (unsigned long) (pow(2, sizeNum) - 1);
	bound = 2*sqrt(pow(2, sizeNum) - 1);
	printf("All Bits:\n");
	printf("\tChiSquare: %f\n", chiSquare );
	printf("\tIdeal: %lu\n", maxNum);
	printf("\tDifference: %.02f%%\n", 100 * fabs(maxNum - chiSquare) / maxNum);
	printf("\tRandom Range: %f to %f \n", maxNum - bound, maxNum + bound );
	printf("\tIsRandom: %d\n", chiSquareRandom);
	printf("------\n");
	
	free(lines);
	free(nums);
	free(buckets);
	free(candidateBits);
	free(candidateNums);
	return 0;
}

