#include <chisquare.h>

double chiSquare_calc(int numLines, int sizeNum, unsigned long* nums)
{
	int i;
	long maxNum;
	double n_r;
	long double chiSquare;
	ulong_int_map frequencies;
	
	maxNum = (int) pow(2, sizeNum) - 1;
	if (numLines < maxNum * 10)
	{
		printf("ERROR: Not Enough Data to perform Chi-Square! numLines (%ld) must be greater than maxNum*10 (%ld)\n", (long) numLines, maxNum*10);
		return 0;
	}
	
	frequencies = chiSquare_getFrequencies(numLines, nums);
	
	n_r = ((double) numLines) / maxNum;
	chiSquare = 0;
	
	for (i = 0; i <= maxNum; i++)
	{
		chiSquare += pow(((double)frequencies[i] - n_r), 2);
	}
	chiSquare /= n_r;
	
	return chiSquare;
}

ulong_int_map chiSquare_getFrequencies(int numLines, unsigned long* nums)
{
	int i;
	ulong_int_map frequencies;
	
	for (i = 0; i < numLines; i++)
	{
		frequencies[nums[i]]++;
	}
	
	return frequencies;
}

int chiSquare_isRandom(long double chiSquare, int sizeNum)
{	
	double r = pow(2, sizeNum) - 1;
	return (fabs(r - chiSquare) <= (long double) 2*sqrt(r) );
}

