import sys


class Result:
    C = [0] * 10
    pValue = -1.0
    proportion = -1.0
    statisticalTest = ""

    def setC(self, index, value):
        self.C[index - 1] = value;

    def setPValue(self, p):
        self.pValue = p

    def setProportion(self, p):
        self.proportion = p

    def setStatisticalTest(self, test):
        self.statisticalTest = test

    def parseResultLine(self, line):
        try:
            splitLine = line.replace("*","").split()
            for i in range(0, 10):
                self.C[i] = int(splitLine[i])
            self.pValue = float(splitLine[10])
            self.proportion = float(splitLine[11])
            self.statisticalTest = splitLine[12]
            return True
        except:
            return False

    def dump(self):
        print self.statisticalTest
        print "\t", self.C
        print "\t", self.pValue
        print "\t", self.proportion

    def printCSV(self):
        print "%s, " % self.statisticalTest,
        for i in range(0, 10):
            print "%f, " % self.C[i],
        print "%f, " % self.pValue,
        print "%f" % self.proportion

def parseResultFile(resultF):
    results = []
    lineNum = 0
    for line in resultF:
        if lineNum > 6:
            result = Result()
            if result.parseResultLine(line):
                results.append(result)
                if result.statisticalTest == "LinearComplexity":
                    break;
        lineNum += 1
    return results


input = sys.argv[1]
resultF = open(input, "r")
results = parseResultFile(resultF)

prevStatTest = ""
print "statisticalTest, C1, C2, C3, C4, C5, C6, C7, C8, C9, C10, pValue, proportion"
for result in results:
    if result.statisticalTest != prevStatTest:
        result.printCSV()
        prevStatTest = result.statisticalTest
