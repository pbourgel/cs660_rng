#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <tr1/unordered_map>

typedef std::tr1::unordered_map<unsigned int, int> ulong_int_map;

double        chiSquare_calc(int numLines, int sizeNum, unsigned long* nums);
int           chiSquare_isRandom(long double chiSquare, int sizeNum);
ulong_int_map chiSquare_getFrequencies(int numLines, unsigned long* nums);