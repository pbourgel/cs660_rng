#include <stdio.h>
#include <stdlib.h>

void readFile(FILE *fp, int numLines, char **lines);
void readLine(FILE* fp, char* line);

void writeCandidateBits(FILE* fp, int sizeNum, int numLines, unsigned long* nums);