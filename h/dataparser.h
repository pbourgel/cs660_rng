#include <stdio.h>
#include <stdlib.h>
#include <math.h>

void linesToNums(int numLines, char **lines, short sizeNum, unsigned long* nums);
void bucketing(int numLines, int sizeNum, unsigned long* nums, int* buckets);
void findCandidateBits(int numLines, short sizeNum, int* buckets, int* candidateBits);
void buildCandidateNums(int numLines, int tolerance, unsigned long* nums, int* candidateBits, unsigned long* candidateNums);