import sys

f = open('out.txt', 'w')
bitlength = int(sys.argv[1])
for i in range(10 * 2**bitlength):
    numstring = bin(i % 2 ** bitlength)[2:]
    while len(numstring) < bitlength:
        numstring = '0' + numstring
    f.write(numstring + "\n")
