#include <stdio.h>

int main(int argc, char** argv)
{
    if (argc < 2)
    {
        printf("./run FileName removeCharacter\n");
        return 0;
    }
    FILE* in = fopen(argv[1], "r");
    FILE* out = fopen("out.txt", "w");
    char c;
    char target = argv[2][0];
    if (target == 'n')
        target = '\n';
    if (target == 't')
	    target = '\t';
    if (target == 's')
        target = ' ';
    if (target == 'm')
        target = 0x0d;
    printf("Char: %c ||\n", target);
    while (!feof(in))
    {
        c = fgetc(in);
        if (c != target)
            fputc(c, out);
    }
    fclose(in);
    fclose(out);
    return 0;
}
