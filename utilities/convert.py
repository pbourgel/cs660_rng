import sys

fin = open(sys.argv[1], 'rb')
base = int(sys.argv[2])
fout = open('out.txt', 'w')

lines = fin.readlines()
seen = 0
bitlength = 8
for line in lines:
    linesplit = line[:-1].split(' ')
    for i in range(0, len(linesplit)):
        word = linesplit[i]
        numstring = bin(int(word, base))[2:]
        while len(numstring) < bitlength:
            numstring = '0' + numstring
        fout.write(numstring + "\n")
        seen += 1
print seen
