#include <stdio.h>
#include <stdlib.h>

int main(int argc, char** argv)
{
    if (argc < 2)
    {
        printf("./run inFile\n");
        return 0;
    }
    FILE* in = fopen(argv[1], "r");
    int count = 0;
    int ones = 0;
    int zeros = 0;

    while (!feof(in))
    {
        if (fgetc(in) == '1')
            ones++;
        else
            zeros++;
        count++;

    }
    printf("\tCount: %d bits\n", count);
    printf("\t\tZeros: %f, Ones: %f\n", (double)zeros / count, (double)ones / count);
    printf("\tStreams: %.2f streams\n", (double)count / 10000);
    return count;
}
