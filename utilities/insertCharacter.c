#include <stdio.h>
#include <stdlib.h>

int main(int argc, char** argv)
{
    if (argc < 3)
    {
        printf("./run inFile inChar skip\n");
        return 0;
    }
    FILE* in = fopen(argv[1], "r");
    FILE* out = fopen("out.txt", "w");
    char c;
    char target = argv[2][0];
    int skip = atoi(argv[3]);
    if (target == 'n')
        target = '\n';
    if (target == 's')
        target = ' ';
    int i = 0;
    while (!feof(in))
    {
        if (i == skip)
        {
            i = 0;
            fputc(target, out);
        }
        c = fgetc(in);
        fputc(c, out);
        i++;
    }
    return 0;
}
