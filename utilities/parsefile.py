import sys

fin = open(sys.argv[1], 'rb')
fout = open('out.txt', 'w')

lines = fin.readlines()
seen = 0
bitlength = 18
for line in lines:
    if seen > 10 * 2**18:
        break;
    linesplit = line.split(' ')
    for i in range(4, len(linesplit), 2):
        word = linesplit[i]
        if i == len(linesplit):
            word = word[:2]
        numstring = bin(int(linesplit[i]))[2:]
        while len(numstring) < bitlength:
            numstring = '0' + numstring
        fout.write(numstring + "\n")
        seen += 1
print seen
