#!/bin/sh

for file in `find RandomData/ -maxdepth 1 -not -type d -and -not -name -'.*'`
do
    echo $(basename "$file")
    eval "./count $file"
done
